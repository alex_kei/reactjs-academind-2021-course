import React, { useState } from 'react';
import Expenses from './components/Expense/Expenses';
import AddExpense from './components/ExpenseControl/AddExpense';

import mockData from './mock-data';

const expenseItems = mockData.map(
  ({id, date, title, price }) => ({ id, date: new Date(date), title, price }),
);

export default function App() {
  const [expenseList, setExpenseList] = useState(expenseItems);
  const addExpenseHandler = (expenseData) => {
    console.log('Application recieved new expense:', expenseData);
    setExpenseList((currentList) => [expenseData, ...currentList]);
  };

  return (
    <div>
      <h2>Let's get started!</h2>
      <AddExpense onAddExpense={addExpenseHandler} />
      <Expenses items={expenseList} />
    </div>
  );
};

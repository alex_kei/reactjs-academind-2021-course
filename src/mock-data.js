const mockData = [
  {
    id: 'ei01',
    date: '2019-05-28',
    title: 'Swimming Suit',
    price: '142.95',
  },
  {
    id: 'ei02',
    date: '2021-04-16',
    title: 'Diving Mask',
    price: '75.68',
  },
  {
    id: 'ei03',
    date: '2020-05-08',
    title: 'Flippers',
    price: '93.55',
  },
  {
    id: 'ei04',
    date: '2021-03-17',
    title: 'Inflatable Boat',
    price: '997.92',
  },
];

export default mockData;

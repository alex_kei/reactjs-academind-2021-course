import React from 'react';
import './Card.css'

export default function Card({ children, className }) {
  const classList = ['card', className].join(' ');
  return (
    <div className={classList}>
      {children}
    </div>
  );
};
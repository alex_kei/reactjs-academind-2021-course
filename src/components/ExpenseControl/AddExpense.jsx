import React, { useState } from 'react';
import ExpenseForm from './ExpenseForm';
import './AddExpense.css';

export default function AddExpense({ onAddExpense }) {
  const [ isControllVisible, setIsControllVisible ] = useState(false);
  const submitHandler = (expense) => {
    onAddExpense({
      ...expense,
      id: Math.random(),
    });
  };

  const showControl = () => setIsControllVisible(true);
  const hideControl = () => setIsControllVisible(false);

  const displayControl = () => {
    if (isControllVisible) {
      return (
        <ExpenseForm
          onSubmitUpdate={submitHandler}
          onCancelUpdate={hideControl}
        />
      );
    }
    return (
      <button type='button' onClick={showControl}>
        Add Expense Item
      </button>
    );
  }

  return (
    <div className='new-expense'>
      {displayControl()}
    </div>
  );
};
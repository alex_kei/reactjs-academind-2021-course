import React, { useState } from 'react';
import './ExpenseForm.css';

export default function ExpenseForm({ onSubmitExpense, onCancelUpdate }) {
  const [title, setTitle] = useState('');
  const [price, setPrice] = useState('');
  const [date, setDate] = useState('');

  const resetInputs = () => {
    setTitle('');
    setPrice('');
    setDate('');
  };

  const changeHandler = ({target}, setter) => {
    setter(target.value);
  };

  const submitHandler = (event) => {
    event.preventDefault();
    const expense = {
      title,
      price,
      date: new Date(date),
    };
    resetInputs();
    onSubmitExpense(expense);
  };

  return (
    <form onSubmit={submitHandler}>
      <div className='new-expense__controls'>
        <div className='new-expense__control'>
          <label>Title</label>
          <input
            type='text'
            value={title}
            onChange={(event) => changeHandler(event, setTitle)}
          />
        </div>
        <div className='new-expense__control'>
          <label>Price</label>
          <input
            type='number'
            min='0'
            max='1000'
            step='0.01'
            value={price}
            onChange={(event) => changeHandler(event, setPrice)}
          />
        </div>
        <div className='new-expense__control'>
          <label>Date</label>
          <input
            type='date'
            min='2019-01-01'
            max='22-12-31'
            value={date}
            onChange={(event) => changeHandler(event, setDate)}
          />
        </div>
      </div>
      <div className='new-expense__actions'>
        <button type='button' onClick={onCancelUpdate}>Cancel</button>
        <button type='submit'>Add Expense</button>
      </div>
    </form>
  );
};
import React from 'react';
import ExpenseItem from './ExpenseItem';
import './ExpenseList.css';

export default function ExpenseList({ items }) {
  if (items.length === 0) {
    return <h2 className='expense-list__fallback'>No expenes found.</h2>
  }

  return (
    <ul className='expense-list'>
      {items.map(
        ({id, ...expenseProps}) => (
          <li key={id}>
            <ExpenseItem { ...expenseProps } />
          </li>
        )
      )}
    </ul>
  );
};

import React, { useState } from 'react';
import Card from '../UI/Card';
import ExpenseListFilter from '../ExpenseFilter/ExpenseListFilter';
import ExpenseList from './ExpenseList';
import './Expenses.css';

const yearsRange = ['2019', '2020', '2021', '2022'];
const [ defaultYear ] = yearsRange;

export default function Expenses({ items }) {
  const [ selectedYear, setSelectedYear ] = useState(defaultYear);

  const filteredItems = items.filter(
    ({ date }) => date.getFullYear() === Number(selectedYear)
  );

  const filterUpdateHandler = (year) => {
    console.log('ExpenseList recieved selected year', year);
    setSelectedYear(year);
  };

  return (
    <Card className='expenses'>
      <ExpenseListFilter
        onFilterUpdate={filterUpdateHandler}
        optionsList={yearsRange}
        selected={selectedYear}
      />
      <ExpenseList items={filteredItems} />
    </Card>
  );
};
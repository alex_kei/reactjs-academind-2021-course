import React from 'react';
import './ExpenseDate.css';

const locale = 'en-US';

export default function ExpenseDate({ date }) {
  const month = date.toLocaleString(locale, { month: 'long' });
  const day = date.toLocaleString(locale, { day: '2-digit' });
  const year = date.getFullYear();

  return (
    <div className='expense-date'>
      <div className='expense-date__month'>{month}</div>
      <div className='expense-date__year'>{year}</div>
      <div className='expense-date__day'>{day}</div>
    </div>
  );
}
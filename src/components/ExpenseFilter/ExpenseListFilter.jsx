import React from 'react';

import './ExpenseListFilter.css';

export default function ExpensesFilter({
  onFilterUpdate,
  optionsList,
  selected,
}) {
  const yearCahngeHandler = ({target}) => {
    onFilterUpdate(target.value);
  };

  return (
    <div className='expenses-filter'>
      <div className='expenses-filter__control'>
        <label>Filter by year</label>
        <select value={selected} onChange={yearCahngeHandler}>
          {optionsList.map((year, index) => <option key={index} value={year}>{year}</option>)}
        </select>
      </div>
    </div>
  );
};
